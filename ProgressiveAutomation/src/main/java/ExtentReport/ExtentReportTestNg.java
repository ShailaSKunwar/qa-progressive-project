package ExtentReport;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportTestNg {
	
	WebDriver driver;
	ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReportTestNg.html");
	
	@BeforeSuite
	public void setUpTest() {
		
	}
	
	@BeforeTest
	public void beforetest() {
		System.setProperty("webdriver.chrome.driver", ".\\library\\chromedriver.exe");
		
		driver = new ChromeDriver();
		}
	
	@Test(priority =0)
	public void ProgressiveTest() {
		
	
		//2. create extent report and attach to the reporter
				
				ExtentReports extent = new ExtentReports();
				
				extent.attachReporter(htmlReporter);
				
		//3. create toggle for given test
				
				ExtentTest test = extent.createTest("Progressive Home Page", "This test checks if the progressive home page opens");
				
		
		 test.info("executing progressive test");
		 
		 driver.get("https://www.progressive.com/home/home/");
				
				if (driver.getTitle().contains("Progressive")) {
				 //msz to pass the condition
				test.pass("Navigated to progressive webpage");
				
					
				}
				
				driver.manage().window().maximize();
				
				
				test.log(Status.INFO, "Ending Progressive Page Test");
				
				
				
			
				
				
				
			
		
//3. create toggle for given test
		
		ExtentTest test2 = extent.createTest("google web Page", "This test checks simple google test");
		
 test2.info("executing google test");
		 
		 driver.get("https://www.google.com/");
				
				if (driver.getTitle().contains("google")) {
				 //msz to pass the condition
				test2.pass("Navigated to google webpage");
				} 
				
				driver.manage().window().maximize();
			
				
				test2.log(Status.INFO, "Ending google Page Test");
				
				test2.fail("this test is to check fail");
				
			
				
				//5. create flush which will write everything to extent reports.
				
				extent.flush();
				
		
		
		
		
	}
	
	@AfterTest
	public void Terminate() {
		driver.close();
		driver.quit();
		
	}

}
