package ExtentReport;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ProgressiveExtentReports {
	
	static WebDriver driver;
	

	public static void main(String[] args) {
		
		
		//1. create extent report and extent html file
		
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReport.html");
		
		
		//2. create extent report and attach to the reporter
		
		ExtentReports extent = new ExtentReports();
		
		extent.attachReporter(htmlReporter);
		
		
		//3. create toggle for given test
		ExtentTest test = extent.createTest("Progressive Home Page", "This test checks if the progressive home page opens");
		
		System.setProperty("webdriver.chrome.driver", ".\\library\\chromedriver.exe");
		
		driver = new ChromeDriver();
		
		
		 //4. start executing test cases
		
		test.log(Status.INFO, "Executing Progressive Page Test");
		
		driver.get("https://www.progressive.com/home/home/");
		
		if (driver.getTitle().contains("Progressive")) {
		 //msz to pass the condition
		test.pass("Navigated to progressive webpage");
		} else {
			test.fail("Wrong WebPage Launched");
			
		}
		
		driver.manage().window().maximize();
		
		
		test.log(Status.INFO, "Ending Progressive Page Test");
		
		test.fail("this test is to check fail message");
		
		driver.quit();
		
		//5. create flush which will write everything to extentreports.
		
		extent.flush();
		
		
		
		

	}

}
