package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DriversInfo {
	public WebDriver driver;

	// constructor to create obj.

	public DriversInfo(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}

	// Locating webelements by xpath

	@FindBy(xpath = "(//input[@type='radio'])[2]")
	WebElement gender;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_MaritalStatus']/option[2]")
	WebElement maritalStatus;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']/option[7]")
	WebElement educationLevel;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_EmploymentStatus']/option[3]")
	WebElement employmentStatus;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_PrimaryResidence']/option[2]")
	WebElement primaryResidence;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_HasPriorAddress']/option[2]")
	WebElement movedAround;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_LicenseStatus']/option[1]")
	WebElement licenseStatus;

	@FindBy(xpath = "//select[@id= 'DriversAddPniDetails_embedded_questions_list_DriverYearsLicensed']/option[2]")
	WebElement yearsLicensed;

	@FindBy(xpath = "//input[@id= 'DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
	WebElement vehicleAccidents;

	@FindBy(xpath = "//input[@id= 'DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
	WebElement ticketsViolation;

	@FindBy(xpath = "//loading-button[@data-automation-id= 'forwardNavigation']")
	WebElement continueBtn;

	public void clientInfo() throws InterruptedException {

		Thread.sleep(3000);
		gender.click();
		maritalStatus.click();

		Thread.sleep(3000);

		educationLevel.click();

		Thread.sleep(3000);

		employmentStatus.click();
		employmentStatus.click();
		
		Thread.sleep(3000);
		primaryResidence.click();
		movedAround.click();

		Thread.sleep(3000);

		licenseStatus.click();
		yearsLicensed.click();
		vehicleAccidents.click();
		ticketsViolation.click();
		continueBtn.click();

	}

}
