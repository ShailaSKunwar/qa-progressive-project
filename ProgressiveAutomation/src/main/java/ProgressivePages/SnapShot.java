package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SnapShot {
	
	public WebDriver driver;

	// constructor to create obj.

	public  SnapShot(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}
	
	//Locating webelements by xpath
	
	@FindBy(xpath= "//input[@id= 'SnapshotEnrollment40Edit_embedded_questions_list_SnapshotPolicyEnrollment_N']")
	WebElement snapShotEnroll;
	
	@FindBy(xpath=  "//loading-button[@data-automation-id= 'forwardNavigation']")
	WebElement continueBtn;
	
	public void SnapshotEnroll() throws InterruptedException {
		
		Thread.sleep(3000);
		
		snapShotEnroll.click();
		
		continueBtn.click();
		
		
		
	}
	

}
