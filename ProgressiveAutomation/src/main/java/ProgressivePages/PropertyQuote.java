package ProgressivePages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PropertyQuote {

	public WebDriver driver;
	
	public void clickElement(WebElement element) {   // How to make this METHOD accessible from all the page class??
	    JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", element);
	}
	// constructor to create obj.

	public PropertyQuote  (WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement
                                                     // Can we initialize Webdriver and Page Factory in base class and use from there
		PageFactory.initElements(driver, this);
	}
	
	//Locating webelements by xpath
	
	@FindBy (id = "PolicyData_EffectiveDate1")
	WebElement policyStart;
	
	@FindBy (xpath= "//div[@class= 'icon']")
	WebElement icon;
	
	@FindBy (xpath= "//button[@class='custom-button big-dark submit   ']")
	WebElement nextBtn;
	
	public void propertyInsQuote(String pStart ) throws InterruptedException {
		
		Thread.sleep(3000);
		policyStart.sendKeys(pStart);
		
		clickElement(nextBtn);
		 
	}
	
	
}
