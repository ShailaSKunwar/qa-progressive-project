package ProgressivePages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DemographicsInput {
	public WebDriver driver;
	
	
	//constructor to create obj.
	
	public DemographicsInput(WebDriver driver) {
		this.driver = driver;
		
		
		//Initialize the webelement
		
	PageFactory.initElements(driver, this);
	}

	
	//Locating elements by xpath
	
	@FindBy(xpath= "//input[@name='NameAndAddressEdit_embedded_questions_list_FirstName']")
	WebElement firstName;
	
	@FindBy(xpath= "//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	WebElement lastName;
	
	@FindBy(xpath= "//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	WebElement birthDate;
	
	@FindBy(xpath= "//input[@name='NameAndAddressEdit_embedded_questions_list_MailingAddress']")
	WebElement streetAddress;
	
	@FindBy(xpath = "//loading-button[@data-automation-id='forwardNavigation']")
	WebElement submitQuote;
	
	public void demographicSec(String fName, String lName, String bDate, String sAddress) throws InterruptedException {
		
		firstName.sendKeys(fName);
		lastName.sendKeys(lName);
		birthDate.sendKeys(bDate);
		Thread.sleep(2000);
		birthDate.sendKeys(Keys.TAB);
		Thread.sleep(2000);
		streetAddress.sendKeys(sAddress);
		submitQuote.click();
	}
	
	
}
