package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnterZipCode {

	public WebDriver driver;

	// constructor to create obj.

	public EnterZipCode(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}

	// Locating an element
	@FindBy(xpath = "//input[@id= 'zipCode_overlay']")
	WebElement zipField;

	@FindBy(xpath = "//input[@id= 'qsButton_overlay']")
	WebElement submitBtn;

	public void enterZip(String zip) {
		zipField.sendKeys(zip);
		submitBtn.click();

	}

}
