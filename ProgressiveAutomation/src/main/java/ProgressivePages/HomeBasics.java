package ProgressivePages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomeBasics {

	public WebDriver driver;

	public void clickElement(WebElement element) { // How to make this METHOD accessible from all the page class??
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click()");

	}

	// constructor to create obj.

	public HomeBasics(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}

	// Locating webelements by xpath

	@FindBy(id = "PolicyData_YearsAtAddress")
	WebElement yearOwned;

	@FindBy(xpath = "document.querySelector(\"#interviewContent > div > div > control:nth-child(3) > div > fieldset > div.control.vAlign > drop-down > div > div > select\")")
	WebElement homeStyle;

  @FindBy (xpath= "//select[@aria-describedby= 'PolicyData_PLTypeOfDwelling_error']/option[2]")
	WebElement singleHome;

	@FindBy(xpath = "//input[@id= 'PolicyData_RoofResponsible_false']")
	WebElement exteriorInsured;

	@FindBy(xpath = "//span[@class= 'custom-input']")
	WebElement primaryHome;

	@FindBy(xpath = "//label[@for= 'PolicyData_EligibilityFinancialHardship_false']")
	WebElement homeStatus;

	@FindBy(xpath = "(//span[@class ='custom-input'])[8]")
	WebElement homeBusiness;

	public void basicInfo(String yOwned) throws InterruptedException {

		Thread.sleep(3000);
		yearOwned.sendKeys(yOwned);

		Thread.sleep(5000);
       clickElement(homeStyle);
	
	  Thread.sleep(3000);
		  
		  clickElement(singleHome);
		  
		  exteriorInsured.click(); primaryHome.click(); Thread.sleep(3000);
		  
		  homeStatus.click(); homeBusiness.click();
		  
		 

	}

}
