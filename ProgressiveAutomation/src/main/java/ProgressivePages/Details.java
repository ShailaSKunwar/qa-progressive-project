package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Details {

	public WebDriver driver;

	// constructor to create obj.

	public Details(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}

	// Locating webelements by xpath

	@FindBy(xpath = "//input[@id= 'FinalDetailsEdit_embedded_questions_list_InsuranceToday_Y']")
	WebElement insuranceHist;

	@FindBy(xpath = "//select[@id= 'FinalDetailsEdit_embedded_questions_list_RecentAutoInsuranceCompanyPeriod']/option[3]")
	WebElement currentCompany;

	@FindBy(xpath = "//select[@id= 'FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits']/option[2]")
	WebElement injuryLimits;

	@FindBy(xpath = "//input[@id= 'FinalDetailsEdit_embedded_questions_list_OtherPolicies_N']")
	WebElement nonAutoPolociy;

	@FindBy(xpath = "//input[@id= 'FinalDetailsEdit_embedded_questions_list_PriorProgressive_N']")
	WebElement autoInsuHist;

	@FindBy(xpath = "//input[@id= 'FinalDetailsEdit_embedded_questions_list_AdvancedShopperPolicyEffectiveDate']")
	WebElement startUrPolicy;

	@FindBy(xpath = "//input[@id= 'FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
	WebElement emailAddress;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_TotalResidents']/option[2]")
	WebElement residents;

	@FindBy(xpath = "//select[@id='FinalDetailsEdit_embedded_questions_list_TotalPipClaimsCount']/option[1]")
	WebElement injuryClaims;

	@FindBy(xpath = "//loading-button[@data-automation-id= \"forwardNavigation\"]")
	WebElement continueBtn;

	public void clientDetails(String date, String email) throws InterruptedException {

		Thread.sleep(5000);
		insuranceHist.click();
		currentCompany.click();

		
		Thread.sleep(3000);
		injuryLimits.click();

		
		Thread.sleep(3000);
		nonAutoPolociy.click();

		
		Thread.sleep(3000);
		autoInsuHist.click();

		Thread.sleep(3000);
		startUrPolicy.sendKeys(date);
		emailAddress.sendKeys(email);
		
		
		Thread.sleep(3000);
		residents.click();

		
		Thread.sleep(2000);
		injuryClaims.click();
		continueBtn.click();

	}

}
