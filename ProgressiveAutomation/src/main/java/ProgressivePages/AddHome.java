package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddHome {
	
	public WebDriver driver;

	// constructor to create obj.

	public  AddHome(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}
	
	//Locating webelements by xpath
	
	
	@FindBy(xpath= "(//span[@class= 'product-button-text'])[1]")
	WebElement addHome;
	

	@FindBy(xpath=  "//loading-button[@data-automation-id= 'forwardNavigation']")
	WebElement continueBtn;
	
	
	public void addHome() throws InterruptedException {
		
		Thread.sleep(2000);
		
		addHome.click();
		
		continueBtn.click();
		
	}
	
	
	

}
