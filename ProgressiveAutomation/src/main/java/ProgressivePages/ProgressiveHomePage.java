package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProgressiveHomePage {

	public WebDriver driver;

	// constructor to create obj.

	public ProgressiveHomePage(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);

	}

	// Locate all elements

	// annotations

	@FindBy(xpath = "(//p[@class= 'txt'])[1]")
	WebElement HomePageAuto;

	public void clickAuto() {
		HomePageAuto.click();

	}

}
