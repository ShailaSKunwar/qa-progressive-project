package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VehicleDiscription {
	public WebDriver driver;

	// constructor to create obj.

	public VehicleDiscription(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}

	// Locating webelements by xpath

	@FindBy(xpath = "//list-input[@id= 'VehiclesNew_embedded_questions_list_Year']/ul/li[4]")

	WebElement vehicleYear;

	@FindBy(xpath = "//list-input[@id= 'VehiclesNew_embedded_questions_list_Make']/ul/li[9]")

	WebElement vehicleMake;

	@FindBy(xpath = "//list-input[@id= 'VehiclesNew_embedded_questions_list_Model']/ul/li[3]")

	WebElement vehicleModel;

	@FindBy(xpath = "//select[@id= 'VehiclesNew_embedded_questions_list_BodyStyle']/option[2]")

	WebElement bodyType;

	@FindBy(xpath = "//select[@id= 'VehiclesNew_embedded_questions_list_VehicleUse']/option[2]")

	WebElement primaryUse;

	@FindBy(xpath = "//select[@id= 'VehiclesNew_embedded_questions_list_OwnOrLease']/option[3]")

	WebElement ownOrLease;

	@FindBy(xpath = "//select[@id= 'VehiclesNew_embedded_questions_list_LengthOfOwnership']/option[4]")

	WebElement vehicleOwned;

	@FindBy(xpath = "//input[@id= 'VehiclesNew_embedded_questions_list_AutomaticEmergencyBraking_Y']")

	WebElement emergencyBraking;

	@FindBy(id = "VehiclesNew_embedded_questions_list_BlindSpotWarning_Y")

	WebElement blindSpot;

	@FindBy(xpath = "(//loading-button[@data-automation-id= 'forwardNavigation'])[1]")

	WebElement doneBtn;

	@FindBy(xpath = "//loading-button[@data-automation-id= 'forwardNavigation']") // data -automation-id does not
																					// support: needs special case

	WebElement continueBtn;

	public void vehicleDiscrip() throws InterruptedException {
		
		Thread.sleep(3000);

		vehicleYear.click();
		vehicleMake.click();
		vehicleModel.click();
		bodyType.click();

		Thread.sleep(3000);
		primaryUse.click();

		Thread.sleep(3000);
		ownOrLease.click();
		vehicleOwned.click();

		Thread.sleep(3000);
		emergencyBraking.click();
		blindSpot.click();

		Thread.sleep(3000);
		doneBtn.click();

		Thread.sleep(3000);
		continueBtn.click();

	}

}
