package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Structure {
	public  WebDriver driver;

	// constructor to create obj.

		public Structure (WebDriver driver) {
			this.driver = driver;

			// Initialize the webelement

			PageFactory.initElements(driver, this);
		}
		
		//Locating webelements by xpath
		
		@FindBy (id= "PolicyData_PLYearBuilt")
		WebElement yearBuilt;
		
		@FindBy (id = "PolicyData_PLSquareFootage")
		WebElement squareFoot;
		
		@FindBy (id= "PolicyData_PLConstructionTypechild32")
		WebElement structureType;
		
		@FindBy (id = "PolicyData_PL_NumberOfFloors")
		WebElement numberOfFloors;
		
		@FindBy (id = "PolicyData_PLFloorNumber")
		WebElement condoFloor;
		
		@FindBy (xpath = "//custom-button[@data-automation-id= 'forwardNavigation']")
		WebElement nextBtn;
		
		public void homeStructure(String yBuilt, String sFoot) throws InterruptedException {
			
			Thread.sleep(3000);
			yearBuilt.sendKeys(yBuilt);
			squareFoot.sendKeys(sFoot);
			
			Thread.sleep(3000);
			structureType.click();
			numberOfFloors.click();
			
			Thread.sleep(3000);
			condoFloor.click();
			nextBtn.click();
			
			
			
		}
		
}

