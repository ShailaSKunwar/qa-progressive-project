package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdditionalDrivers {
	public WebDriver driver;

	// constructor to create obj.

	public AdditionalDrivers(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}
	
	//Locating webelements by xpath
	
	@FindBy(xpath=  "//loading-button[@data-automation-id= 'forwardNavigation']")
	WebElement continueBtn;
	
	
	public void policyHolders () throws InterruptedException {                //not sure 
		
		
		Thread.sleep(3000);
		continueBtn.click();
		
		
	}
	
	

}
