package ProgressivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReviewPage {
	public WebDriver driver;

	// constructor to create obj.

	public  ReviewPage(WebDriver driver) {
		this.driver = driver;

		// Initialize the webelement

		PageFactory.initElements(driver, this);
	}
	
	//Locating webelements by xpath
	
	@FindBy(xpath= "//loading-button[@data-automation-id= \"forwardNavigation\"]")
	
	WebElement reviewDemo;
	
	public void reviewInfo() throws InterruptedException {
		
		Thread.sleep(3000);
		
		reviewDemo.click();
	}

}
