package SeleniumUtils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotUtility {

	public static void takeSnapShot(WebDriver driver, String screenShotName) {
		
		try {
			File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyFile(src, new File(".\\Screenshots\\" + screenShotName +".Jpeg"));
			
			Thread.sleep(2000);
			
			System.out.println("screen shot was taken!");
			
			
			
		}catch(Exception e) {
			
			System.out.println("Exception while taking screen shot!!" +e.getMessage());
			e.printStackTrace();
			
			
		}
		

	}

}
