package ProgressiveTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;

import ProgressivePages.AddHome;
import ProgressivePages.AdditionalDrivers;
import ProgressivePages.DemographicsInput;
import ProgressivePages.Details;
import ProgressivePages.DriversInfo;
import ProgressivePages.EnterZipCode;
import ProgressivePages.HomeBasics;
import ProgressivePages.ProgressiveHomePage;
import ProgressivePages.PropertyQuote;
import ProgressivePages.ReviewPage;
import ProgressivePages.SnapShot;
import ProgressivePages.Structure;
import ProgressivePages.VehicleDiscription;
import SeleniumUtils.ScreenShotUtility;

public class StartAutoQuoteTestNg {

	WebDriver driver;
	
	@BeforeTest
	public void BeforeTest() {
		// 1. set the system path

		System.setProperty("webdriver.chrome.driver", ".\\library\\chromedriver.exe");

		// creating the web obj.

		driver = new ChromeDriver();
		
		
	}
	
	public void NavigateToHomePage() {
		try {
		driver.get("https://www.progressive.com/home/home/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		ScreenShotUtility util = new ScreenShotUtility();
		}  catch (NoSuchElementException e) {
			e.getAdditionalInformation();
			e.getCause();
			e.getStackTrace();
			e.getSupportUrl();
		}
		
	}
	
@Test(priority = 0)

	
public void StartAutoQuote() {
	
	NavigateToHomePage();
		try {
			ProgressiveHomePage progressivehomePage = new ProgressiveHomePage(driver);
			

			EnterZipCode enterzipCode = new EnterZipCode(driver);

			DemographicsInput demographicsInput = new DemographicsInput(driver);

			VehicleDiscription vehicleDiscription = new VehicleDiscription(driver);

			DriversInfo driversInfo = new DriversInfo(driver);

			AdditionalDrivers additionalDrivers = new AdditionalDrivers(driver);

			ReviewPage reviewPage = new ReviewPage(driver);

			Details details = new Details(driver);
			

			SnapShot snapShot = new SnapShot(driver);
			
			AddHome addHome = new AddHome(driver);
			
			PropertyQuote propertyQuote = new PropertyQuote(driver);
			
			HomeBasics homeBasics = new HomeBasics(driver);
			
			Structure structure = new Structure(driver);
			
		progressivehomePage.clickAuto();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		enterzipCode.enterZip("76005");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		
		demographicsInput.demographicSec("Blake", "Shelton", "06/18/1976", "123 Viridian Park Ln");
		

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		vehicleDiscription.vehicleDiscrip();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driversInfo.clientInfo();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		additionalDrivers.policyHolders();

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		reviewPage.reviewInfo();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		details.clientDetails("10/26/2020", "ttest@gmail.com");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		snapShot.SnapshotEnroll();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		addHome.addHome();
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		propertyQuote.propertyInsQuote("11/01/2020");
		

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		homeBasics.basicInfo("5");
		
		
		
	} catch (NoSuchElementException e) {
		e.getAdditionalInformation();
		e.getCause();
		e.getStackTrace();
		e.getSupportUrl();
	} catch (Exception e) {
		e.printStackTrace();
		e.getCause();
		e.getMessage();

	}
}

@AfterTest

public void AfterTest() {
	driver.close();
	
	driver.quit();
	
	
}

}

